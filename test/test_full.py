import io
import lzma
import mmap
from hashlib import md5
from pathlib import Path

import lz4.frame
import pytest
from plum import pack
from plum.array import Array
from plum.int import Int

from arx import Archive, IndexType
from arx.directory import ListIndex
from arx.internal import (
    ArxHeader,
    BaseIndexHeader,
    ClusterHeader,
    CompressionType,
    ContentAddress,
    DirectoryHeader,
    EntryPos,
    File,
    HeaderFreeData,
    KeyInfo,
    KeyType,
    ListIndexHeader,
    PackHeader,
    PackInfo,
    PackInfoFreeData,
    UInt64,
)
from arx.pack import Cluster
from utils import to_bytes, to_int


@pytest.fixture(
    params=[CompressionType.NONE, CompressionType.LZ4, CompressionType.LZMA]
)
def compression(request):
    return request.param


@pytest.fixture
def pack_(tmpdir, compression, articles):
    # pylint: disable=redefined-outer-name, too-many-locals
    # The data to store:
    data = b"".join([a[3].encode() for a in articles])
    if compression == CompressionType.LZ4:
        data = lz4.frame.compress(data, store_size=False)
    elif compression == CompressionType.LZMA:
        data = lzma.compress(data)
    # Calculate the offsets of all data
    offset = 0
    offsets = []
    for a in articles:
        offset += len(a[3].encode())
        offsets.append(offset)
    # The size needed to store the offsets
    offsetSize = (offset.bit_length() + 7) // 8

    class offsetType(Int, nbytes=offsetSize, signed=False, byteorder="big"):
        pass

    class OffsetArray(Array, item_cls=offsetType):
        pass

    # The whole cluster len
    clusterSize = ClusterHeader.nbytes + len(articles) * offsetSize + len(data)

    path = "Pack.arxp"
    with open(tmpdir / path, "w+b") as f:
        f.seek(PackHeader.nbytes)
        # Write the entryPtrPos
        entryPtrPos = f.tell()
        for i in range(len(articles)):
            # entry i in cluster 0
            f.write(to_bytes(i, 4))
        # Write the clusterPos
        clusterPtrPos = f.tell()
        f.write(to_bytes(clusterPtrPos + 8, 8))
        # Write the cluster
        f.write(
            ClusterHeader(
                compressionType=compression,
                clusterSize=clusterSize,
                blobCount=len(articles),
                offsetSize=offsetSize,
            ).pack()
        )
        f.write(pack(offsetType, offset))
        f.write(pack(OffsetArray, offsets[:-1]))
        f.write(data)
        checkInfoPos = f.tell()
        f.seek(0)
        f.write(
            PackHeader(
                magic=to_int(b"arxp"),
                vendorId=to_int(b"test"),
                majorV=1,
                minorV=0,
                uuid=to_bytes(0, 16),
                packSize=checkInfoPos + 5,
                checkInfoPos=checkInfoPos,
                entryCount=len(articles),
                clusterCount=1,
                entryPtrPos=entryPtrPos,
                clusterPtrPos=clusterPtrPos,
            ).pack()
        )

        # Write the md5
        m = md5()
        f.seek(0)
        m.update(f.read())
        md5_digest = m.digest()
        f.seek(10)
        f.write(md5_digest)

        # Write the checkInfo
        f.seek(0, io.SEEK_END)
        checkInfo = to_bytes(5, 4) + b"\x00"
        f.write(checkInfo)

    packInfo = PackInfo(
        uuid=md5_digest,
        packId=0,
        freeData=PackInfoFreeData(b""),
        packSize=checkInfoPos + 5,
        packOffset=0,
        packCheckInfoPos=checkInfoPos,
        packPath=path.encode(),
    )
    return tmpdir / path, packInfo, checkInfo


@pytest.fixture
def directory(tmpdir, articles):
    # pylint: disable=too-many-locals
    # Each entry will have 1 name (primary key),
    # 2 meta datas (number of words (2 bytes) and sentences (1 byte)) and 1 content address.
    # We have to store the name in a keydata array.
    offset = 0
    offsets = []
    keysdata = []
    for a in articles:
        offsets.append(offset)
        bname = a[0].encode()
        keysdata.append(to_bytes(len(bname), 1) + bname)
        offset += len(bname) + 1
    keydata = b"".join(keysdata)
    offsetSize = (offset.bit_length() + 7) // 8
    keyInfos = [
        KeyInfo(keySize=offsetSize, keyType=KeyType.pstring),
        KeyInfo(keySize=1, keyType=KeyType.uint),
        KeyInfo(keySize=2, keyType=KeyType.uint),
        KeyInfo(
            keySize=0, keyType=KeyType.contentAddress
        ),  # All zero, so contentAddress
    ]
    headerSize = 42 + len("test") + len(keyInfos)  # keyCount  # keyCount * u8
    path = "directory.arxd"
    with open(tmpdir / path, "w+b") as f:
        f.seek(DirectoryHeader.nbytes)
        indexPtrPos = f.tell()
        # We have a one index offset to write. The index is just after the offset
        f.write(to_bytes(indexPtrPos + 8, 8))
        f.write(
            ListIndexHeader(
                headerSize=headerSize,
                indexKey=1,
                entrySize=offsetSize + 2 + 1 + 4,
                indexLength=len(articles),
                keysDataPos=headerSize,
                keysDataLen=len(keydata),
                indexArrayPos=headerSize + len(keydata),
                extraDataCI=ContentAddress(packId=0, contentId=0),
                indexName=b"test",
                keyInfoArray=keyInfos,
            ).pack()
        )
        f.write(keydata)
        for i, a in enumerate(articles):
            f.write(to_bytes(offsets[i], offsetSize))
            f.write(to_bytes(a[1], 1))
            f.write(to_bytes(a[2], 2))
            f.write(b"\x00" + to_bytes(i, 3))
        checkInfoPos = f.tell()
        f.seek(0)
        f.write(
            DirectoryHeader(
                magic=to_int(b"arxd"),
                vendorId=to_int(b"test"),
                majorV=1,
                minorV=0,
                uuid=b"\x00" * 16,
                packSize=checkInfoPos + 5,
                checkInfoPos=checkInfoPos,
                indexCount=1,
                indexPtrPos=indexPtrPos,
            ).pack()
        )

        # Write the md5
        m = md5()
        f.seek(0)
        m.update(f.read())
        md5_digest = m.digest()
        f.seek(10)
        f.write(md5_digest)

        # Write the checkInfo
        f.seek(0, io.SEEK_END)
        checkInfo = to_bytes(5, 4) + b"\x00"
        f.write(checkInfo)

    packInfo = PackInfo(
        uuid=md5_digest,
        packId=0,
        freeData=PackInfoFreeData(b""),
        packSize=checkInfoPos + 5,
        packOffset=0,
        packCheckInfoPos=checkInfoPos,
        packPath=path.encode(),
    )
    return tmpdir / path, packInfo, checkInfo


@pytest.fixture
def arxFile(tmpdir, pack_, directory):
    # pylint: disable=redefined-outer-name
    _directoryPath, directoryInfo, directoryCheckInfo = directory
    directoryInfo.packCheckInfoPos = ArxHeader.nbytes + 2 * PackInfo.nbytes
    _packPath, packInfo, packCheckInfo = pack_
    packInfo.packCheckInfoPos = ArxHeader.nbytes + 2 * PackInfo.nbytes + 5

    with open(tmpdir / "test.arx", "w+b") as f:
        f.seek(ArxHeader.nbytes)
        f.write(directoryInfo.pack())
        f.write(packInfo.pack())
        f.write(directoryCheckInfo)
        f.write(packCheckInfo)
        checkInfoPos = f.tell()
        f.seek(0)
        f.write(
            ArxHeader(
                magic=to_int(b"arxf"),
                vendorId=to_int(b"test"),
                majorV=1,
                minorV=0,
                uuid=b"\x00" * 16,
                checkInfoPos=checkInfoPos,
                packCount=1,
                freeData=HeaderFreeData(b""),
            ).pack()
        )

        # Write the md5
        m = md5()
        f.seek(0)
        # File Header
        m.update(f.read(ArxHeader.nbytes))
        # Update with the constant part of the packInfo and skip the variable part
        m.update(f.read(136))
        f.seek(120, io.SEEK_CUR)
        m.update(f.read(136))
        f.seek(120, io.SEEK_CUR)
        # Update the packs checkInfo
        m.update(f.read(10))
        md5_digest = m.digest()
        f.seek(10)
        f.write(md5_digest)

        f.seek(0, io.SEEK_END)
        checkInfo = to_bytes(5, 4) + b"\x00"
        f.write(checkInfo)

    return tmpdir / "test.arx"


def test_pack(pack_, articles):
    # pylint: disable=redefined-outer-name
    path, _packInfo, _checkInfo = pack_
    with open(path, "r+b") as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            packHeader = PackHeader.unpack_from(mm, 0)
            assert packHeader.entryCount == len(articles)
            assert packHeader.clusterCount == 1
            for i in range(packHeader.entryCount):
                entryPos = EntryPos.unpack_from(
                    mm, packHeader.entryPtrPos + EntryPos.nbytes * i
                )
                assert entryPos.clusterNumber == 0
                assert entryPos.blobNumber == i
            clusterHeaderPos = UInt64.unpack_from(mm, packHeader.clusterPtrPos)
            _f = File(f)
            cluster = Cluster(_f, clusterHeaderPos)
            assert cluster.blobCount == len(articles)
            for i in range(cluster.blobCount):
                assert cluster.get_blob_data(i) == articles[i][3].encode()


def test_directory(directory, articles):
    # pylint: disable=redefined-outer-name
    path, _packInfo, _checkInfo = directory
    with open(path, "r+b") as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            directoryHeader = DirectoryHeader.unpack_from(mm, 0)
            assert directoryHeader.indexCount == 1
            headerPos = UInt64.unpack_from(mm, directoryHeader.indexPtrPos)
            header = BaseIndexHeader.unpack_from(mm, headerPos)
            assert header.indexType == 0
            index = ListIndex(File(f), headerPos, header.headerSize)
            assert index.header.indexType == 0
            assert index.header.indexLength == len(articles)
            assert index.header.entrySize == 8
            for i, entry in enumerate(index):
                assert entry[0] == articles[i][0].encode()
                assert entry[1] == articles[i][1]
                assert entry[2] == articles[i][2]
                assert entry[3].packId == 0
                assert entry[3].contentId == i


def test_arx_file(arxFile):
    # pylint: disable=redefined-outer-name
    dirpath = Path(arxFile.dirname)
    with open(arxFile, "r+b") as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            arxHeader = ArxHeader.unpack_from(mm, 0)
            assert arxHeader.packCount == 1
            # Some basic test
            for i in range(arxHeader.packCount + 1):
                packInfo = PackInfo.unpack_from(mm, ArxHeader.nbytes + 256 * i)
                packPath = Path(dirpath / packInfo.packPath.decode())
                assert packInfo.packSize == packPath.stat().st_size
                assert packInfo.packOffset == 0
                packCheckInfo = bytes(
                    mm[packInfo.packCheckInfoPos : packInfo.packCheckInfoPos + 5]
                )
                with packPath.open("r+b") as pf:
                    pf.seek(10, io.SEEK_SET)
                    uuid = pf.read(16)
                    pf.seek(-5, io.SEEK_END)
                    checkInfo = pf.read(5)
                assert packInfo.uuid == uuid
                assert packCheckInfo == checkInfo


def test_arx_file_reading(arxFile, articles):
    # pylint: disable=redefined-outer-name
    with Archive.open(arxFile) as archive:
        assert archive.packCount == 1
        assert archive.indexCount == 1
        index = archive.get_index(0)
        assert index.indexType == IndexType.listIndex
        assert len(index) == len(articles)
        for i, entry in enumerate(index):
            assert entry[0] == articles[i][0].encode()
            assert entry[1] == articles[i][1]
            assert entry[2] == articles[i][2]
            contentAddress = entry[3]
            assert contentAddress.packId == 0
            assert contentAddress.contentId == i
            pack = archive.get_pack(contentAddress.packId)
            assert pack.get_content(contentAddress.contentId) == articles[i][3].encode()
