import pytest
from plum import UnpackError

from arx.internal import ArxHeader, PackInfo


@pytest.fixture
def bytesarxheader():
    return (
        b"arxftest\x01\x00"  # magic + version
        + b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"  # uuid
        + b"\xff\xff\xff\xff\xff\xff\xff\xff"  # checkInfo pointer
        + b"\x02"  # 2 pack
        + b"\xee" * 21  # freedata
    )


class TestArxHeader:
    def test_simple(self, bytesarxheader):
        # pylint: disable=redefined-outer-name,no-self-use
        header = ArxHeader.unpack(bytesarxheader)

        assert header.magic == 0x61727866  # b"arxf"
        assert header.vendorId == 0x74657374  # b"test"
        assert header.majorV == 1
        assert header.minorV == 0
        assert header.uuid == bytes(
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        )
        assert header.packCount == 2
        assert header.checkInfoPos == 0xFFFFFFFFFFFFFFFF
        assert header.freeData == bytes([0xEE]) * 21

    def test_truncated(self, bytesarxheader):
        # pylint: disable=redefined-outer-name,no-self-use
        with pytest.raises(UnpackError):
            ArxHeader.unpack(bytesarxheader[:-5])


@pytest.fixture
def bytespackinfo():
    return (
        b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"  # uuid
        + b"\x01"  # packId
        + b"\xee" * 111  # freedata
        + b"\xbb" * 8  # packSize
        + b"\xff" * 8  # packOffset
        + b"\xdd" * 8  # packCheckInfoOffset
        + b"\x00" * 104  # packPath
    )


class TestPackInfo:
    def test_packinfo(self, bytespackinfo):
        # pylint: disable=redefined-outer-name,no-self-use
        packInfo = PackInfo.unpack(bytespackinfo)

        assert packInfo.uuid == bytes(
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        )
        assert packInfo.packId == 1
        assert packInfo.packSize == 0xBBBBBBBBBBBBBBBB
        assert packInfo.packOffset == 0xFFFFFFFFFFFFFFFF
        assert packInfo.packPath == b""
        assert packInfo.freeData == bytes([0xEE]) * 111

    def test_truncated(self, bytespackinfo):
        # pylint: disable=redefined-outer-name,no-self-use
        with pytest.raises(UnpackError):
            PackInfo.unpack(bytespackinfo[:-5])
