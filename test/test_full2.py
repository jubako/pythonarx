import io
import mmap
from pathlib import Path

import pytest

from arx import Archive, IndexType
from arx.creator import Creator
from arx.internal import ArxHeader, KeyType, PackInfo


@pytest.fixture
def arxFile(tmpdir, articles):
    with Creator(tmpdir / "test.arx", vendorId=0, freeData=b"") as creator:
        packSlot = creator.add_packSlot(
            1, [{"path": tmpdir / "pack.arxp", "freeData": b"This is the pack"}],
        )
        index = creator.add_index(
            IndexType.listIndex,
            b"test",
            indexKey=0,
            keys=[KeyType.pstring, KeyType.uint, KeyType.uint, KeyType.contentAddress],
        )
        for name, nbSentences, nbWords, content in articles:
            contentAddress = packSlot.add_content([content.encode()])
            index.add_entry((name, nbSentences, nbWords, contentAddress))

    return tmpdir / "test.arx"


def test_arx_file(arxFile):
    # pylint: disable=redefined-outer-name
    dirpath = Path(arxFile.dirname)
    with open(arxFile, "r+b") as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as mm:
            arxHeader = ArxHeader.unpack_from(mm, 0)
            assert arxHeader.packCount == 1
            # Some basic test
            for i in range(arxHeader.packCount + 1):
                packInfo = PackInfo.unpack_from(mm, ArxHeader.nbytes + 256 * i)
                packPath = Path(dirpath / packInfo.packPath.decode())
                assert packInfo.packSize == packPath.stat().st_size
                assert packInfo.packOffset == 0
                packCheckInfo = bytes(
                    mm[packInfo.packCheckInfoPos : packInfo.packCheckInfoPos + 37]
                )
                with packPath.open("r+b") as pf:
                    pf.seek(10, io.SEEK_SET)
                    uuid = pf.read(16)
                    pf.seek(-37, io.SEEK_END)
                    checkInfo = pf.read(37)
                assert packInfo.uuid == uuid
                assert packCheckInfo == checkInfo


def test_arx_file_reading(arxFile, articles):
    # pylint: disable=redefined-outer-name
    with Archive.open(arxFile) as archive:
        assert archive.packCount == 1
        assert archive.indexCount == 1
        index = archive.get_index(0)
        assert index.indexType == IndexType.listIndex
        assert len(index) == len(articles)
        for i, entry in enumerate(index):
            assert entry[0] == articles[i][0].encode()
            assert entry[1] == articles[i][1]
            assert entry[2] == articles[i][2]
            contentAddress = entry[3]
            pack = archive.get_pack(contentAddress.packId)
            assert (
                bytes(pack.get_content(contentAddress.contentId))
                == articles[i][3].encode()
            )
