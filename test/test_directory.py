import pytest

from arx.directory import ListIndex
from arx.internal import DirectoryHeader, KeyType
from utils import PseudoFile


def to_bytes(number, size):
    return number.to_bytes(size, byteorder="big")


@pytest.fixture
def bytesdirectoryheader():
    return (
        b"arxdtest\x01\x00"  # magic + version
        + b"\x00\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"  # uuid
        + b"\xaa" * 8  # fileSize
        + b"\xff\xff\xff\xff\xff\xff\xff\xff"  # checkInfo pointer
        + b"\x00\x00\x00\x05"  # indexCount
        + b"\xee\xee\xee\xee\xee\xee\xee\xee"  # indexPtrPos pointer
    )


@pytest.fixture(
    params=[
        KeyType.pstring,
        KeyType.pstring | KeyType.localSize,
        KeyType.pstring | KeyType.flookup,
        KeyType.pstring | KeyType.localSize | KeyType.flookup,
    ]
)
def pstringtype(request):
    return request.param


@pytest.fixture(params=[1, 2, 5, 8])
def pstringaddresssize(request):
    return request.param


class TestDirectoryHeader:
    def test_simple(self, bytesdirectoryheader):
        # pylint: disable=redefined-outer-name, no-self-use
        header = DirectoryHeader.unpack(bytesdirectoryheader)

        assert header.magic == 0x61727864  # b"arxd"
        assert header.vendorId == 0x74657374  # b"test"
        assert header.majorV == 1
        assert header.minorV == 0
        assert header.uuid == bytes(
            [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        )
        assert header.packSize == 0xAAAAAAAAAAAAAAAA
        assert header.indexCount == 5
        assert header.indexPtrPos == 0xEEEEEEEEEEEEEEEE
        assert header.checkInfoPos == 0xFFFFFFFFFFFFFFFF

    def test_truncated(self, bytesdirectoryheader):
        # pylint: disable=redefined-outer-name, no-self-use
        with pytest.raises(TypeError):
            DirectoryHeader(bytesdirectoryheader[:-5])


class TestListIndex:
    def test_simple(self):
        # pylint: disable=no-self-use
        headerSize = 42 + 4 + 2
        buf = (
            (b"\x00" + to_bytes(headerSize, 2))  # baseIndex
            + b"\x00"  # (no) primary key
            + to_bytes(5 + 3, 1)  # Sizeof (u5 + u3)
            + b"\x00" * 3  # padding
            + b"\x00\x00\x00\x04"  # indexLength (4 entries)
            + b"\x00" * 16  # No keyDataPos and keyDataLen
            + to_bytes(42 + 4 + 2, 8)  # indexArrayPos
            + b"\x01\x00\x00\xff"  # extra data Content Address (pack 1, content 0xff)
            + b"\x04test"  # indexName
            + b"\x02"  # keyCount
            + b"\x15\x13"  # keys type
            + (b"\x01\x02\x03\x04\x05" + b"\x01\x02\x03")  # entry 1
            + (b"\x11\x12\x13\x14\x15" + b"\x11\x12\x13")  # entry 2
            + (b"\x21\x22\x23\x24\x25" + b"\x21\x22\x23")  # entry 3
            + (b"\x31\x32\x33\x34\x35" + b"\x31\x32\x33")  # entry 4
        )

        index = ListIndex(PseudoFile(buf), 0, headerSize)

        assert index.header.indexType == 0
        assert index.header.indexKey == 0
        assert index.header.entrySize == 8
        assert index.header.indexLength == 4
        assert index.header.keysDataPos == 0
        assert index.header.keysDataLen == 0
        assert index.header.indexArrayPos == 48
        extraData = index.header.extraDataCI
        assert extraData.packId == 1
        assert extraData.contentId == 0xFF
        assert index.header.indexName == b"test"

        assert index.entryType.nbytes == 8

        assert len(index) == 4
        for entryIndex, entry in enumerate(index):
            key0Value = 0
            for i in range(1, 6):
                key0Value = (key0Value << 8) + (entryIndex << 4) + i
            key1Value = 0
            for i in range(1, 4):
                key1Value = (key1Value << 8) + (entryIndex << 4) + i

            assert entry[0] == key0Value
            assert entry[1] == key1Value

    def test_exporteddata(self, pstringtype, pstringaddresssize):
        # pylint: disable=redefined-outer-name, no-self-use, too-many-locals, too-many-statements
        firstChar = (b"V", b"L", b"V", b"V")
        if pstringtype == KeyType.pstring:
            datakey = b"\x05Value\x09LongValue\x0aValue2and3"
            keysOffset = (0, 6, 16, 16)
            pstringkeysize = pstringaddresssize
        elif pstringtype == KeyType.pstring | KeyType.localSize:
            datakey = b"LongValue2and3"
            keysOffset = (4, 0, 4, 4)
            keysSize = (5, 9, 10, 10)
            pstringkeysize = pstringaddresssize + 1
        elif pstringtype == KeyType.pstring | KeyType.flookup:
            datakey = b"\x04alue\x08ongValue\x09alue2and3"
            keysOffset = (0, 5, 14, 14)
            pstringkeysize = pstringaddresssize + 1
        else:
            datakey = b"ongValue2and3"
            keysOffset = (4, 0, 4, 4)
            keysSize = (4, 8, 9, 9)
            pstringkeysize = pstringaddresssize + 2
        entries = []
        for i in range(0, 4):
            entrydata = [bytes([(i << 4) + j for j in range(1, 4)])]
            if pstringtype & KeyType.localSize:
                entrydata.append(bytes([keysSize[i]]))
            if pstringtype & KeyType.flookup:
                entrydata.append(firstChar[i])
            entrydata.append(
                keysOffset[i].to_bytes(pstringaddresssize, byteorder="big")
            )
            entries.append(b"".join(entrydata))

        headerSize = 42 + 4 + 2
        buf = (
            (b"\x00" + to_bytes(headerSize, 2))  # baseIndex
            + b"\x00"  # (no) primary key
            + to_bytes(3 + pstringkeysize, 1)  # Sizeof (u5 + u3)
            + b"\x00" * 3  # padding
            + b"\x00\x00\x00\x04"  # indexLength (4 entries)
            + to_bytes(48, 8)  # keyDataPos
            + to_bytes(len(datakey), 8)  # keyDataLen
            + to_bytes(48 + len(datakey), 8)  # indexArrayPos (44)
            + b"\x01\x00\x00\xff"  # extra data Content Address (pack 1, content 0xff)
            + b"\x04test"  # indexName
            + b"\x02\x13"
            + bytes([(pstringtype << 4) + pstringkeysize])  # keys type
            + datakey
            + b"".join(entries)
        )

        index = ListIndex(PseudoFile(buf), 0, headerSize)

        assert index.header.indexType == 0
        assert index.header.indexKey == 0
        assert index.header.entrySize == 3 + pstringkeysize
        assert index.header.indexLength == 4
        assert index.header.keysDataPos == 48
        assert index.header.keysDataLen == len(datakey)
        assert index.header.indexArrayPos == 48 + len(datakey)
        extraData = index.header.extraDataCI
        assert extraData.packId == 1
        assert extraData.contentId == 0xFF
        assert index.header.indexName == b"test"

        assert index.entryType.nbytes == 3 + pstringkeysize

        assert len(index) == 4
        for entryIndex, entry in enumerate(index):
            key0Value = 0
            for i in range(1, 4):
                key0Value = (key0Value << 8) + (entryIndex << 4) + i

            if entryIndex == 0:
                key1Value = b"Value"
            elif entryIndex == 1:
                key1Value = b"LongValue"
            else:
                key1Value = b"Value2and3"

            assert entry[0] == key0Value
            assert entry[1] == key1Value
