import pytest
from plum import PackError, UnpackError, pack

from arx.internal import PString


def test_pstring():
    buf = b"\x05abcdefghijklmnopqr\x05stuvw"
    assert PString.unpack_from(buf) == b"abcde"
    assert PString.unpack_from(buf[:6]) == b"abcde"
    assert PString.unpack_from(buf, 19) == b"stuvw"

    class FixedSizePString(PString, nbytes=10):
        pass

    assert FixedSizePString.unpack_from(buf) == b"abcde"
    assert FixedSizePString.unpack_from(buf[:10]) == b"abcde"

    with pytest.raises(UnpackError):
        FixedSizePString.unpack_from(buf[:9])
    with pytest.raises(UnpackError):
        PString.unpack_from(buf[:3])
    with pytest.raises(UnpackError):
        PString.unpack_from(buf[:22], 19)
    with pytest.raises(UnpackError):

        class SmallFixedSizePString(PString, nbytes=5):
            pass

        SmallFixedSizePString.unpack_from(buf)


@pytest.fixture(params=[b"", b"a", b"abcde", b"abcdefghijkl", b"abcde\x00abcde"])
def content(request):
    _content = request.param
    return (_content, bytes([len(_content)]) + _content)


def test_pstring_pack(content):
    # pylint: disable=redefined-outer-name
    content, result = content
    assert PString(content).pack() == result
    assert pack(PString, content) == result

    class FixedSizePString(PString, nbytes=10):
        pass

    if len(content) <= 9:
        assert FixedSizePString(content).pack() == result + b"\x00" * (10 - len(result))
        assert pack(FixedSizePString, content) == result + b"\x00" * (10 - len(result))
    else:
        with pytest.raises(ValueError):
            FixedSizePString(content).pack()
        with pytest.raises(PackError):
            pack(FixedSizePString, content)
