import io
from contextlib import contextmanager
from pathlib import Path

from .directory import Directory
from .internal import ArxHeader, File, PackInfo
from .pack import Pack


class Archive:
    @classmethod
    @contextmanager
    def open(cls, path):
        with File.open(path) as f:
            a = cls(path, f)
            yield a
            a.close()

    def __init__(self, path, f):
        self.path = Path(path)
        self.file = f
        self.fileSize = f.file.seek(0, io.SEEK_END)
        self.header = ArxHeader.unpack(f.read(0, ArxHeader.nbytes))
        self.packArray = self.file.createArray(
            ArxHeader.nbytes + 256, PackInfo, self.header.packCount
        )
        self._directory = None
        self._packCache = {}

    def close(self):
        if self._directory is not None:
            self._directory.close()
        for pack in self._packCache.values():
            pack.close()

    @property
    def packCount(self):
        return self.header.packCount

    @property
    def indexCount(self):
        return self.get_directory().indexCount

    def _get_pack(self, packInfo, Type):
        if packInfo.packOffset:
            assert packInfo.packOffset + packInfo.packSize < self.fileSize
            return Type(self.path, packInfo.packOffset)
        path = self.path.parent / packInfo.packPath.decode()
        if path.exists():
            return Type(path, 0)
        raise KeyError("Cannot found pack")

    def get_pack(self, packId):
        if packId not in self._packCache:
            for packInfo in self.packArray:
                if packInfo.packId != packId:
                    continue
                try:
                    self._packCache[packId] = self._get_pack(packInfo, Pack)
                    break
                except KeyError:
                    pass

        return self._packCache[packId]

    def get_directory(self):
        if self._directory is None:
            packInfo = PackInfo.unpack(self.file.read(ArxHeader.nbytes, 256))
            self._directory = self._get_pack(packInfo, Directory)
        return self._directory

    def get_index(self, indexNumber):
        directory = self.get_directory()
        return directory.get_index(indexNumber)
