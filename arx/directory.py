import enum

from .internal import (
    BaseIndexHeader,
    DirectoryHeader,
    File,
    ListIndexHeader,
    UInt64,
    createEntryForListIndex,
)


class Directory:
    def __init__(self, path, pos):
        self.file = File(open(path, "r+b"))
        self.pos = pos
        self.header = DirectoryHeader.unpack(self._fread(0, DirectoryHeader.nbytes))
        self.indexOffsetArray = self.file.createArray(
            pos + self.header.indexPtrPos, UInt64, self.header.indexCount
        )

    def close(self):
        self.file.close()

    def _fread(self, offset, length):
        return self.file.read(self.pos + offset, length)

    def get_index(self, indexNumber):
        headerOffset = self.indexOffsetArray[indexNumber]
        header = BaseIndexHeader.unpack(
            self._fread(headerOffset, BaseIndexHeader.nbytes)
        )
        if header.indexType == 0:
            return ListIndex(self.file, self.pos + headerOffset, header.headerSize)
        raise RuntimeError()

    @property
    def indexCount(self):
        return self.header.indexCount


class IndexType(enum.IntEnum):
    listIndex = 0


class ListIndex:
    indexType = IndexType.listIndex

    def __init__(self, file, pos, headerSize):
        self.file = file
        self.pos = pos
        self.header = ListIndexHeader.unpack(self.file.read(pos, headerSize))
        assert self.header.indexType == self.indexType
        self.entryType = createEntryForListIndex(self.header, self)
        assert self.header.entrySize == self.entryType.nbytes
        if self.header.keysDataLen:
            globalPos = self.pos + self.header.keysDataPos
            self._keydata = self.file.memview(globalPos, self.header.keysDataLen)
        else:
            self._keydata = None
        self.entryArray = self.file.createArray(
            self.pos + self.header.indexArrayPos,
            self.entryType,
            self.header.indexLength,
        )

    def get_keydata(self):
        assert self._keydata is not None
        return self._keydata

    def __len__(self):
        return self.header.indexLength

    def __getitem__(self, index):
        return self.entryArray[index]
