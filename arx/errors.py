class ArxError(Exception):
    """Base Exception raise by arx library"""


class ArxFormatError(ArxError):
    pass
