import io
from pathlib import Path
from uuid import uuid4

from ..directory import IndexType
from ..internal import DirectoryHeader, PackInfo, PackInfoFreeData, UInt64
from .checkInfo import CheckInfo
from .listIndex import ListIndex


class Directory:
    def __init__(self, path, vendorId):
        self.path = Path(path)
        self.file = open(path, "w+b")
        self.header = DirectoryHeader(
            vendorId=vendorId,
            uuid=uuid4().bytes,
            packSize=0,
            checkInfoPos=0,
            indexCount=0,
            indexPtrPos=0,
        )
        self.indexes = []

    def add_index(self, indexType, name, **kwords):
        if indexType == IndexType.listIndex:
            index = ListIndex(name, **kwords)
        self.indexes.append(index)
        return index

    def finalize(self):
        f = self.file
        self.header.indexCount = len(self.indexes)
        indexOffset = []
        f.seek(DirectoryHeader.nbytes)
        self.header.indexPtrPos = f.tell()
        f.seek(len(self.indexes) * 8, io.SEEK_CUR)
        for index in self.indexes:
            indexOffset.append(f.tell())
            index.write(f)
        f.seek(self.header.indexPtrPos, io.SEEK_SET)
        for offset in indexOffset:
            f.write(UInt64.pack(offset))
        f.seek(0, io.SEEK_END)
        self.header.checkInfoPos = f.tell()
        f.seek(0)
        f.write(self.header.pack())

        checkInfo = CheckInfo(f)
        checkInfo.read(self.header.checkInfoPos)
        f.write(bytes(checkInfo))

        packInfo = PackInfo(
            uuid=self.header.uuid,
            packId=0,
            packSize=f.tell(),
            freeData=PackInfoFreeData(b""),
            packOffset=0,
            packCheckInfoPos=self.header.checkInfoPos,
            packPath=bytes(self.path),
        )
        f.close()

        return packInfo, checkInfo
