import io
from pathlib import Path
from typing import List
from uuid import uuid4

from ..internal import ArxHeader, ContentAddress, HeaderFreeData
from .checkInfo import CheckInfo
from .directory import Directory
from .pack import Pack


class Creator:
    def __init__(self, path, *, vendorId, freeData):
        self.path = Path(path)
        self.file = open(path, "w+b")
        print(self.file)
        self.header = ArxHeader(
            vendorId=vendorId,
            uuid=uuid4().bytes,
            checkInfoPos=0,
            packCount=0,
            freeData=HeaderFreeData(freeData),
        )
        self.packs = {}
        self.directory = Directory(f"{path}.dir", vendorId)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self.finalize()
        finally:
            self.close()

    def add_packSlot(self, slotId, packs):
        self.packs[slotId] = [
            Pack(packId=slotId, vendorId=self.header.vendorId, **p) for p in packs
        ]
        return SlotWrapper(self, slotId)

    def add_index(self, indexType, name, **kwords):
        return self.directory.add_index(indexType, name, **kwords)

    def finalize(self):
        packInfos = []
        for packSlots in self.packs.values():
            for pack in packSlots:
                packInfos.append(pack.finalize())
        self.header.packCount = len(packInfos)
        dirInfo = self.directory.finalize()
        f = self.file
        f.seek(ArxHeader.nbytes + 256 * (len(packInfos) + 1))
        dirInfo[0].packCheckInfoPos = f.tell()
        f.write(bytes(dirInfo[1]))
        for packInfo in packInfos:
            packInfo[0].packCheckInfoPos = f.tell()
            f.write(bytes(packInfo[1]))
        self.header.checkInfoPos = f.tell()

        f.seek(0)
        f.write(self.header.pack())
        f.write(dirInfo[0].pack())
        for packInfo in packInfos:
            f.write(packInfo[0].pack())

        checkInfo = CheckInfo(f)
        checkInfo.read(ArxHeader.nbytes)
        for _ in range(len(packInfos) + 1):
            checkInfo.read(136)
            checkInfo.skip(120)
        checkInfo.read(self.header.checkInfoPos - dirInfo[0].packCheckInfoPos)
        f.seek(0, io.SEEK_END)
        f.write(bytes(checkInfo))

    def close(self):
        self.file.close()


class SlotWrapper:
    def __init__(self, arxFile, _id):
        self.arxFile = arxFile
        self._id = _id

    def add_content(self, contents: List[bytes]):
        contentId = None
        for pack, content in zip(self.arxFile.packs[self._id], contents):
            cId = pack.add_content(content)
            if contentId is None:
                contentId = cId
            assert contentId == cId
        return ContentAddress(packId=self._id, contentId=contentId)
