from plum.array import Array
from plum.bytearray import ByteArray
from plum.int import Int
from plum.int.bitfields import BitField, BitFields
from plum.structure import (
    BitFieldMember,
    DimsMember,
    Member,
    SizeMember,
    Structure,
    VariableDimsMember,
    VariableSizeMember,
)

from .base import CompressionType, KeyType
from .types import (
    ContentAddress,
    HeaderFreeData,
    PackInfoFreeData,
    PackInfoPathName,
    UInt8,
    UInt16,
    UInt24,
    UInt32,
    UInt40,
    UInt48,
    UInt56,
    UInt64,
    Uuid,
)
from .utils import to_int


class PackHeader(Structure):
    magic: int = Member(cls=UInt32, default=to_int(b"arxp"))
    vendorId: int = Member(cls=UInt32)  # offset 4
    majorV: int = Member(cls=UInt8, default=1)  # offset 8
    minorV: int = Member(cls=UInt8, default=0)  # offset 9
    uuid = Member(cls=Uuid)  # offset 10
    packSize: int = Member(cls=UInt64)  # offset 26
    checkInfoPos: int = Member(cls=UInt64)  # offset 34
    entryCount: int = Member(cls=UInt24)
    clusterCount: int = Member(cls=UInt24)
    entryPtrPos: int = Member(cls=UInt64)
    clusterPtrPos: int = Member(cls=UInt64)


class ClusterType(BitFields, nbytes=1, byteorder="big", ignore=0xF0):  # type: ignore
    compressionType: CompressionType = BitField(size=4)


TYPE_MAP = {
    1: UInt8,
    2: UInt16,
    3: UInt24,
    4: UInt32,
    5: UInt40,
    6: UInt48,
    7: UInt56,
    8: UInt64,
}


class ClusterHeader(Structure):
    @property
    def offsetType(self):
        # pylint: disable=no-member
        return TYPE_MAP[self.offsetSize]

    compressionType: CompressionType = BitFieldMember(cls=ClusterType)
    clusterSize: int = Member(cls=UInt64)
    blobCount: int = Member(cls=UInt16)
    offsetSize: int = Member(cls=UInt8)


class DirectoryHeader(Structure):
    magic: int = Member(cls=UInt32, default=to_int(b"arxd"))
    vendorId: int = Member(cls=UInt32)  # offset 4
    majorV: int = Member(cls=UInt8, default=1)  # offset 8
    minorV: int = Member(cls=UInt8, default=0)  # offset 9
    uuid = Member(cls=Uuid)  # offset 10
    packSize: int = Member(cls=UInt64)  # offset 26
    checkInfoPos: int = Member(cls=UInt64)  # offset 34
    indexCount: int = Member(cls=UInt32)
    indexPtrPos: int = Member(cls=UInt64)


class BaseIndexHeader(Structure):
    indexType: int = Member(cls=UInt8)
    headerSize: int = Member(cls=UInt16)


class BaseKeyString(ByteArray):
    # pylint: disable=no-member
    @classmethod
    def __unpack__(cls, buffer, offset, parent, dump):
        # pylint: disable=arguments-differ
        if cls.localSize:
            size, offset = UInt8.__unpack__(
                buffer,
                offset,
                parent,
                None
                if dump is None
                else dump.add_record(access="[0] (.size)", cls=UInt8),
            )
        else:
            size = None
        if cls.flookup:
            first, offset = buffer[offset : offset + 1], offset + 1
        else:
            first = b""

        koffset, offset = cls.offset.__unpack__(
            buffer,
            offset,
            parent,
            None
            if dump is None
            else dump.add_record(access="[2] (.offset)", cls=cls.offset),
        )

        keyData = cls.index.get_keydata()
        if size is None:
            size, koffset = UInt8.__unpack__(keyData, koffset, parent, None)
        data = keyData[koffset : koffset + size]
        return first + data, offset

    @classmethod
    def __pack__(cls, buffer, offset, parent, value, dump):
        # pylint: disable=too-many-arguments
        size = len(value) - int(cls.flookup)
        if cls.localSize:
            offset = UInt8.__pack__(buffer, offset, parent, size, dump)
        if cls.flookup:
            buffer[offset : offset + 1] = value[0]
            offset += 1
            if dump:
                dump.add_record(access="flookup", value=str(value[0]), memory=value[0])
        cls.offset.__pack__(buffer, offset, parent, value.offset, dump)


class KeyInfo(BitFields, nbytes=1, byteorder="big"):  # type: ignore
    keySize: int = BitField(size=4)
    keyType: KeyType = BitField(size=4)

    def getKeyClass(self, baseName):
        if self.keyType == KeyType.contentAddress and self.keySize == 0:
            return ContentAddress
        if self.keyType in (KeyType.uint, KeyType.sint):
            kName = (
                baseName
                + "."
                + "US"[self.keyType == KeyType.sint]
                + "Int"
                + str(self.keySize)
            )
            kClass = type(
                kName,
                (Int,),
                {},
                nbytes=self.keySize,
                signed=self.keyType == KeyType.sint,
                byteorder="big",
            )
            return kClass
        if self.keyType == KeyType.char_array:
            kName = baseName + ".Array" + str(self.keySize)
            kClass = type("kClass", (ByteArray,), {}, nbytes=self.keySize)
            return kClass

        # pstring
        ksize = self.keySize
        flookup = False
        localSize = False
        if self.keyType & 0b1:  # localSize
            localSize = True
            ksize -= 1
        if self.keyType & 0b10:  # flookup
            flookup = True
            ksize -= 1
        kName = (
            baseName
            + ".KString_"
            + ("lSize_" if localSize else "")
            + ("flookup_" if flookup else "")
            + str(ksize)
        )
        offset = type(
            "kOffset", (Int,), {}, nbytes=ksize, signed=False, byteorder="big"
        )
        kClass = type(
            kName,
            (BaseKeyString,),
            {"localSize": localSize, "flookup": flookup, "offset": offset},
            nbytes=self.keySize,
        )
        return kClass


class KeyInfoArray(Array, item_cls=KeyInfo):  # type: ignore
    pass


class ListIndexHeader(Structure):
    indexType: int = Member(cls=UInt8, default=0)
    headerSize: int = Member(cls=UInt16)
    indexKey: int = Member(cls=UInt8)  # offset: 3
    entrySize: int = Member(cls=UInt8)  # offset: 4
    _padding = Member(cls=UInt24, ignore=True, default=0)  # offset: 5
    indexLength: int = Member(cls=UInt32)  # offset: 8
    keysDataPos: int = Member(cls=UInt64)  # offset: 12
    keysDataLen: int = Member(cls=UInt64)  # offset: 20
    indexArrayPos: int = Member(cls=UInt64)  # offset: 28
    extraDataCI = Member(cls=ContentAddress)  # offset: 36
    _indexNameSize: int = SizeMember(cls=UInt8)  # offset: 40
    indexName: bytes = VariableSizeMember(
        size_member=_indexNameSize, cls=ByteArray
    )  # offset: 41
    keyCount: int = DimsMember(cls=UInt8)  # offset 41+_indexNameSize
    keyInfoArray: list = VariableDimsMember(
        dims_member=keyCount, cls=KeyInfoArray
    )  # offset 42+_indexNameSize
    # Size 42+_indexNameSize+keyCount


class ArxHeader(Structure):
    magic: int = Member(cls=UInt32, default=to_int(b"arxf"))
    vendorId: int = Member(cls=UInt32)  # offset 4
    majorV: int = Member(cls=UInt8, default=1)  # offset 8
    minorV: int = Member(cls=UInt8, default=0)  # offset 9
    uuid = Member(cls=Uuid)  # offset 10
    checkInfoPos: int = Member(cls=UInt64)  # offset 26
    packCount: int = Member(cls=UInt8)
    freeData: bytes = Member(cls=HeaderFreeData)


class PackInfo(Structure):
    uuid = Member(cls=Uuid)
    packId: int = Member(cls=UInt8)
    freeData: bytes = Member(cls=PackInfoFreeData)
    packSize: int = Member(cls=UInt64)
    packOffset: int = Member(cls=UInt64)
    packCheckInfoPos: int = Member(cls=UInt64)
    packPath: bytes = Member(cls=PackInfoPathName)
