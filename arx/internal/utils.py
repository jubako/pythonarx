import io
import mmap
from contextlib import contextmanager


def to_bytes(number, size):
    return number.to_bytes(size, byteorder="big")


def to_int(_bytes):
    return int.from_bytes(_bytes, byteorder="big")


class Array:
    def __init__(self, file, offset, item_type, item_count):
        self.file = file
        self.offset = offset
        self.item_type = item_type
        self.item_count = item_count

    def __len__(self):
        return self.item_count

    def __getitem__(self, index):
        if index >= self.item_count:
            raise IndexError
        offset = self.offset + self.item_type.nbytes * index
        return self.item_type.unpack(self.file.read(offset, self.item_type.nbytes))


class File:
    @classmethod
    @contextmanager
    def open(cls, path):
        with io.open(path, "r+b") as _f:
            f = cls(_f)
            yield f
            f.close()

    def __init__(self, file):
        self.file = file
        self.memviews = {}

    def close(self):
        for view, _mmap in reversed(list(self.memviews.values())):
            view.release()
            _mmap.close()

    def memview(self, offset, size):
        mmview = self.memviews.get((offset, size))
        if mmview is not None:
            return mmview[0]

        inMmapOffset = offset % mmap.ALLOCATIONGRANULARITY
        mmapOffset = offset - inMmapOffset
        _mmap = mmap.mmap(
            self.file.fileno(),
            size + inMmapOffset,
            access=mmap.ACCESS_READ,
            offset=mmapOffset,
        )
        memview = memoryview(_mmap)[inMmapOffset:]
        self.memviews[(offset, size)] = (memview, _mmap)
        return memview

    def read(self, offset, size):
        self.file.seek(offset)
        return self.file.read(size)

    def createArray(self, offset, item_type, item_count):
        return Array(self, offset, item_type, item_count)
